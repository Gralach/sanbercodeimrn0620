function DescendingTen(input)
{
  var string = "";
  if (input == null)
  {
    return -1;
  }

  for(x = 0; x < 10; x++)
  {
    string = string + ` ${input}`;
    input = input -1
  }
  return string
}

// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

function AscendingTen(input)
{
  var string = "";
  if (input == null)
  {
    return -1;
  }
  
  for(x = 0; x < 10; x++)
  {
    string = string + ` ${input}`;
    input = input +1;
  }
  return string
}

// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

function ConditionalAscDesc(reference, check)
{
  var string = "";
  if (reference == null || check == null)
  {
    return -1;
  }
  else 
  {
    if (check%2==0)
    {
      for(x = 0; x < 10; x++)
      {
        string = string + ` ${reference}`;
        reference = reference -1;
      }
      return string
    }
    else
    {
      for(x = 0; x < 10; x++)
      {
        string = string + ` ${reference}`;
        reference = reference +1;
      }
      return string
    }
  }
}

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

function ularTangga()
{
  var start = 100;
  for (x = 0; x<10; x++)
  {
    var string = "";
    for (y = 0; y<10; y++)
    {
      string = string + ` ${start}`;
      start = start -1;
    }
    console.log(string)
  }
}
ularTangga()