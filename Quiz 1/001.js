//Soal A
function bandingkan(num1,num2)
{
  if(num1 === -1 || num2 === -1)
  {
    return -1
  }
  else if (num1>num2)
  {
    return num1
  }
  else if (num1<num2)
  {
    return num2
  }  
  else
  {
    return -1
  }
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18


//Soal B
function balikString(input)
{
  var string =  "";
  for(x=input.length; x>= 0; x--)
  {
    string = string + input.substring(x,x-1);
  }
  return string;
}

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

//Soal C
function palindrome(input)
{
  string = balikString(input)
  if(string === input)
  {
    return true;
  }
  else 
  {
    return false
  }
}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false