//Soal 1
function range(startNum,finishNum)
{
  let array= [];
  if(!!startNum&&!!finishNum)
  {
    if (startNum<finishNum)
    {
      for(startNum; startNum<= finishNum; startNum++)
      {
        array.push(startNum);
      }
      return array;
    }
    else if (startNum>finishNum)
    {
      for(startNum; startNum>= finishNum; startNum--)
      {
        array.push(startNum);
      }
      return array;
    }
  }
  else 
  {
    return -1;
  }
};

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//Soal 2
function rangeWithStep(startNum,finishNum,step=1)
{
  let array= [];
  if (startNum<finishNum)
    {
    for(let i = startNum; i <= finishNum; i+=step)
    {
      array.push(i);
    }
    return array;
  }
  else
  {
    for(let i = startNum; i >= finishNum; i-=step)
    {
      array.push(i);
    }
    return array;
  }

};

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//Soal 3
function sum(startNum=0,finishNum=0,step=1)
{
  var sum = 0;
  if (startNum<finishNum)
    {
    for(let i = startNum; i <= finishNum; i+=step)
    {
      sum = sum + i;
    }
    return sum;
  }
  else
  {
    for(let i = startNum; i >= finishNum; i-=step)
    {
      sum = sum + i;
    }
    return sum;
  }
};
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//Soal 4
function dataHandling(input)
{
  var string = "";
  for(x=0; x< input.length; x++)
  {
     string  = string + `Nomor ID:  ${input[x][0]}\nNama Lengkap:  ${input[x][1]}\nTTL:  ${input[x][2]}\nHobi:  ${input[x][3]}\n\n`;
  }
  return string;
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]
console.log(dataHandling(input))

//Soal 5
function balikKata(input)
{
  var string =  "";
  for(x=input.length; x>= 0; x--)
  {
    string = string + input.substring(x,x-1);
  }
  return string;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal 6
function dataHandling2(input)
{
  input.splice(1,4,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung","21/05/1989","Pria","SMA Internasional Metro")
  return input;
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
var newinput = dataHandling2(input)
console.log(newinput)

var ttl = newinput[3].split("/");

switch(ttl[1]) {
 case '01': console.log("Januari");break;
 case '02': console.log("Februari"); break;
 case '03': console.log("Maret"); break;
 case '04': console.log("April"); break;
 case '05': console.log("Mei"); break;
 case '06': console.log("Juni"); break;
 case '07': console.log("Juli"); break;
 case '08': console.log("Agustus"); break;
 case '09': console.log("September"); break;
 case '10': console.log("Oktober"); break;
 case '11': console.log("November"); break;
 case '12': console.log("Desember"); break;
}
console.log(ttl)
var ttl = ttl.join("-")
console.log(ttl)
newinput[3] = ttl;

newinput[1] = newinput[1].slice(0,14)
console.log(newinput[1])