//Soal 1
function arrayToObject(arr)
{
  var object = {};
  var now = new Date()
  var thisYear = now.getFullYear() // 2020 (tahun sekarang)
  for (i = 0; i < arr.length; i++)
  {
    var nama = `${i+1}. ${arr[i][0]} ${arr[i][1]}`;
    if (arr[i][3] !== undefined && (thisYear-arr[i][3]) > 0)
    {
      object[nama] = 
      {
        firstName : arr[i][0],
        lastName : arr[i][1],
        gender : arr[i][2],
        age : `${thisYear - arr[i][3]}`
      }
    }
    else
    {
      object[nama] =
      {
        firstName : arr[i][0],
        lastName : arr[i][1],
        gender : arr[i][2],
        age : `Invalid Birth Year`
      }
    }
  }
  return object;
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log(arrayToObject(people))

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log(arrayToObject(people2)) 

//Soal 2
function shoppingTime(memberId, money)
{

  if (memberId == undefined)
  {
    return "Mohon maaf, toko X hanya berlaku untuk member saja"
  }
  else if (money < 50000)
  {
    return "Mohon maaf, uang tidak cukup"
  }
  else
  {
    var object = {};
    object.memberId = memberId;
    object.money = money;
    array = [[1500000,500000,250000,175000,50000],['Sepatu Stacattu', 'Baju Zoro','Baju H&N','Sweater Uniklooh','Casing Handphone']]
    kosong= []
    for(var x = 0; x<array[0].length;x++)
    {
      if (money >= array[0][x])
      {
        money = money - array[0][x];
        kosong.push(array[1][x]);
      }
    }
    object.listPurchased = kosong;
    object.changeMoney = money
    return object;
  }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal 3
function naikAngkot(arrPenumpang)
{
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var angkot = [{},{}];
  var i=0;
  var asal = '';
  var tujuan = '';
  for (var i=0; i<arrPenumpang.length; i++) 
  {
    for (var j=0; j<arrPenumpang[i].length; j++) 
    {
              switch (j) {
              case 0: {
                  angkot[i].penumpang = arrPenumpang[i][j];
                  break;
              } case 1: {
                  angkot[i].naikDari = arrPenumpang[i][j];
                  angkot[i].tujuan = arrPenumpang[i][j+1];
                  break;
              } case 2: {
                  asal = arrPenumpang[i][j-1];
                  tujuan = arrPenumpang[i][j];
                  var jarak = 0;
                  for (var k=0; k<rute.length; k++) {
                      if (rute[k] === asal) {
                          for (var l=k+1; l<rute.length; l++) {
                              jarak += 1;
                              if (rute[l] === tujuan) {
                                  var bayar = jarak * 2000;
                                  angkot[i].bayar = bayar;
                              }
                          }
                      }
                  }
                  break;
              }
          }
    }
  }
  return angkot;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));