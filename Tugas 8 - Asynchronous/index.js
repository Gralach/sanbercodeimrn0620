var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let index = 0;
function read(times) {
  if (index < books.length) {
    readBooks(times, books[index], (sisaWaktu) => read(sisaWaktu));
  }
  index++;
}

read(10000);
