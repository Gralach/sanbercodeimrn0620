var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var i = 0;
function read(times) {
  if (i < books.length)
    readBooksPromise(times, books[i])
      .then((result) => read(result))
      .catch((error) => console.log(error));
  i++;
}

read(10000);
