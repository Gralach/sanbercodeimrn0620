//Soal 1
var count = 1;
var pertama = "I love coding";
var kedua = "I will become a mobile doveloper"
console.log(`LOOPING PERTAMA`)
while(count<=20)
{
  if (count%2==0)
  {
    console.log(`${count} - ${pertama}`)
  }
  count++;
}
console.log(`LOOPING KEDUA`)
while(count>1)
{
  if (count%2==0)
  {
    console.log(`${count} - ${kedua}`)
  }
  count--;
}

//Soal 2
var ganjil = "Santai";
var genap = "Berkualitas";
var tiga = "I Love Coding";

for(x = 1; x<=20; x++)
{
  if (x%2==0)
  {
    console.log(`${x} - ${genap}`)
  }
  else if (x%3 == 0)
  {
    console.log(`${x} - ${tiga}`)
  }
  else
  {
    console.log(`${x} - ${ganjil}`)
  }
}

// Soal 3
for(x=0;x<4;x++)
{
  console.log('########')
}

// Soal 4
var hash = "#";
for(x = 1; x < 8; x++)
{
  console.log(hash.repeat(x))
}

// Soal 5
var hash = "# # # #";
for(x = 1; x < 8; x++)
{
  if(x%2==0)
  {
    console.log(hash)
  }
  else
  {
    console.log(" "+ hash)
  }
}